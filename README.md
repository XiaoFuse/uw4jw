An Unofficial Webapp for jw.org website
The official website of Jehovah's Witnesses. It provides online access to the Bible, Bible-based publications, and current news. It also describes our beliefs and organization.

JW Broadcasting
Free Christian movies, educational and entertaining programs for families, teens, children, all. Official JW broadcast.

Watchtower Online Library
This is a research tool for publications in various languages produced by Jehovah’s Witnesses.