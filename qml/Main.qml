import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import QtWebEngine 1.7
import Ubuntu.Components 1.3 as Ubuntu
import Morph.Web 0.1

import "Components" as Components

ApplicationWindow {
    id: root

    visible: true
    width: units.gu(50)
    height: units.gu(75)

    Settings {
        id: settings
        property string lastUrl: 'https://jw.org'
        property string language
        property int defaultAnimeList: 0
        property int defaultMangaList: 0
    }

    WebContext {
        id: webcontext
        userAgent: 'Mozilla/5.0 (Linux; Android 8.0; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.151 Mobile Safari/537.36 Ubuntu Touch Webapp'
        offTheRecord: false
    }

    WebView {
        id: webview
        anchors {
            top: parent.top
            bottom: nav.top
        }
        width: parent.width
        height: parent.height

        context: webcontext
        url: settings.lastUrl
        onUrlChanged: {
            var strUrl = url.toString();
            if (settings.lastUrl != strUrl && strUrl.match('(http|https)://jw.org/(.*)')) {
                settings.lastUrl = strUrl;
            }
        }

        function navigationRequestedDelegate(request) {
            var url = request.url.toString();
            var isvalid = false;

            if (!url.match('(http|https)://jw.org/(.*)') && request.isMainFrame) {
                Qt.openUrlExternally(url);
                request.action = WebEngineNavigationRequest.IgnoreRequest;
            }
        }
    }

    ProgressBar {
        height: units.dp(3)
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        value: (webview.loadProgress / 100)
        visible: (webview.loading && !webview.lastLoadStopped)
    }

    // TODO conver this to be qqc2/suru style
    Components.BottomNavigationBar {
        id: nav

        function loadPage() {
            if (settings.language) {
                var url = 'https://jw.org/' + settings.language;
                return url;
            }

            return null;
        }

        selectedIndex: -1 // Don't show any items as active
        model: [
            {
                'name': i18n.tr('Home'),
                'iconName': 'home',
                'url':loadPage(),
            },
            {
                'name': i18n.tr('Settings'),
                'iconName': 'settings',
                'url': null,
            }
        ]

        onTabThumbClicked: {
            if (model[index].url) {
                webview.url = model[index].url;
            }
            else {
                settingsDialog.open()
            }
        }
    }

    Dialog {
        id: settingsDialog

        width: parent.width - units.gu(10)

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        parent: ApplicationWindow.overlay

        modal: true
        title: i18n.tr('Settings')

        function save() {
            settings.language = user.text;
            settingsDialog.close();
        }

        ColumnLayout {
            spacing: units.gu(1)
            anchors.fill: parent

            Label {
                text: i18n.tr('Site Language')
            }

            TextField {
                id: user
                Layout.fillWidth: true

                text: settings.language

                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                onAccepted: settingsDialog.save()
            }

            Button {
                Layout.fillWidth: true
                Layout.topMargin: units.gu(1)
                Layout.bottomMargin: units.gu(1)
                text: i18n.tr('OK')

                onClicked: settingsDialog.save()
            }
        }
    }

    Connections {
        target: Ubuntu.UriHandler
        onOpened: {
            webview.url = uris[0];
        }
    }

    Component.onCompleted: {
        if (Qt.application.arguments[1] && Qt.application.arguments[1].indexOf('jw.org') >= 0) {
            webview.url = Qt.application.arguments[1];
        }
    }
}
